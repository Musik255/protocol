import UIKit



//Почитать про copy on write (CoW) и понять, что это такое
//Реализовать структуру IOSCollection и создать в ней copy on write
func address(off value: AnyObject) -> String{
    print(Unmanaged.passUnretained(value).toOpaque())
    return "\(Unmanaged.passUnretained(value).toOpaque())"

}

func address(of object: UnsafeRawPointer) -> String {
    let address = Int(bitPattern: object)
    print(address)
    return String(format: "%p", address)

}





final class Ref<T> {
    var value: T
    init(_ value: T) {
        self.value = value
    }
}

struct ContainerReferenceStruct<T>{

    var reference : Ref<T>
    init(_ structRef: T) {
        reference = Ref(structRef)
    }

    var value: T {
        get {
            reference.value
        }
        set {
            if (!isKnownUniquelyReferenced(&reference)) {
                print("More ref \(isKnownUniquelyReferenced(&reference))")
                reference = Ref(newValue)
        }
            else {
                print("one ref")
                reference.value = newValue

            }
    }
  }
}


struct IOSCollection {
    var x : Int
    init (x : Int){
        self.x = x
    }
}

let struct1 = IOSCollection(x: 5)

var container = ContainerReferenceStruct(struct1)

var container2 = container

address(off: container.reference)
address(off: container2.reference)

container2.value.x = 10



address(off: container.reference)
address(off: container2.reference)





//Создать протокол *Hotel* с инициализатором, который принимает roomCount, после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол
protocol Hotel{
    init (roomCount : Int)
}


class HotelAlfa : Hotel{
    
    var roomCount : Int
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}



//Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так что б когда мы напишем такую конструкцию "let diceCoub = 4 \n diceCoub.numberDice" в консоле мы увидели такую строку - "Выпало 4 на кубике"



protocol GameDice{
    var numberDice: String { get }
}

extension Int : GameDice{
    var numberDice: String {
        print("Выпало \(self) на кубике")
        return "Выпало \(self) на кубике"
    }
}

let diceCoub = 4
diceCoub.numberDice



//Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional, создать класс, подписать на протокол и реализовать только 1 обязательное свойство


protocol NoteBookFrame{
    func openCover() -> Void
    var keyboardLayout : String { get set }
    var drive : String? { get set }
}

class Asus : NoteBookFrame{
    func openCover() {
        print("Cover is open")
    }
    var keyboardLayout: String
    var drive: String?
    init(_ keyboardLayout : String){
        self.keyboardLayout = keyboardLayout
    }
    init(_ keyboardLayout : String, _ drive : String) {
        self.keyboardLayout = keyboardLayout
        self.drive = drive
    }
}
var keyboard = "qwerty"
var asusA_480 = Asus(keyboard)


//Создать 2 протокола:
//3.1 - "Начинай писать код" со свойствами: время, количество кода. И функцией writeCode()
//3.2 - "Заканчивай писать код" с функцией: stopCoding()
//И класс: Разработчик, у которого есть свойства - количество программистов, специализации(ios, android, web).
//Разработчику подключаем два этих протокола.
//Задача: вывести в консоль сообщения - "разработка началась. пишем код" и "работа закончена. Сдаю в тестирование".
//

protocol StartWritingCode{
    var time : Int {get set}
    var codeAmount : Int {get set}
    func writeCode() -> Void
}

protocol StopWritingCode{
    func stopCoding() -> Void
}

class Company : StartWritingCode, StopWritingCode{
    var time: Int = 0
    var codeAmount: Int = 0
    var developers : Int
    enum Specialization{
        case ios
        case android
        case web
    }
    var specialization : Specialization
    
    func writeCode() {
        print("Разработка началась. пишем код")
    }
    
    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование")
    }
    init(developers : Int, specialization : Specialization) {
        self.developers = developers
        self.specialization = specialization
    }
    
}


var company = Company(developers: 10, specialization: .ios)
company.writeCode()
company.stopCoding()
